---
authors:
- iacovoni
bio: 
first_name: Jason
last_name: Iacovoni
organizations:
- name: I2MC
role: IR
social:
- icon: globe
  icon_pack: fas
  link: 
superuser: false
title: Jason Iacovoni
user_groups:
- Researchers
external_link: 
---


