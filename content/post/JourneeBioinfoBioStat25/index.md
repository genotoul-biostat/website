---
date: "2025-02-20"
title: 6 nov. 2025 - Journée GenoToul Bioinfo / Biostat 
---

La prochaine journée commune des plateformes Biostatistique et Bioinformatique de GenoToul aura lieu le 06 novembre 2025 à INRAE (site d'Auzeville). 

Pour plus d'informations : [https://bioinfo-biostat.sciencesconf.org/](https://bioinfo-biostat.sciencesconf.org/)

<!--more-->

Biostatistics and Bioinformatics play a key role in biology research with a spectacular development in recent years due to technological advances. This meeting is part of a series of meetings and exchanges between regional teams in bioinformatics and biostatistics. It offers to researchers and engineers the opportunity to present their activity on methodological or technical developments. It also gives the opportunity to biologists to present original results obtained from the application of bioinformatics/biostatistics methods. It follows the events organized in December 2009, March 2011, March 2012, June 2014, June 2015, December 2016, December 2018, October 2019, October 2021, December 2022, November 2023, November 2024. The 2020 meeting was canceled due to COVID.

This year the meeting will be held on **Thursday, November 6th** at INRAE, Auzeville center, Amphithéâtre du bâtiment PAPS-B. 
<!--It will be maintained as a virtual meeting if sanitary conditions prevent the face-to-face meeting.-->
