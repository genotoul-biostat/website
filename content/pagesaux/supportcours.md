---
title: Supports de cours
type: page
date: 
---

Cette page recense des supports de formation en (bio)statistique mis à votre disposition par les animateurs/trices de la plateforme de biostatistique de Toulouse. 

<h2>Introduction à la statistique</h2>

- [supports de cours pour « Introduction à la statistique »](http://www.nathalievialaneix.eu/teaching/biostat1.html)  (formation annuelle PF biostat, Sébastien Déjean, Nathalie Vialaneix et Sandrine Laguerre)

<h2>Statistique générale</h2>

- [statistique exploratoire multidimensionnelle](http://www.math.univ-toulouse.fr/~sdejean/PDF/stat_multidim_sdejean.pdf) (classification ascendante hiérarchique, k-moyennes, ACP, AFD, AFC, régression linéaire multiple) (Sébastien Déjean)

- [tutoriel de recommandation pour le modèle mixte sous R](http://www.nathalievialaneix.eu/doc/html/guidelines_modele_mixte.html) (Laurent Cauquil et Sylvie Combes, juin 2018)
- [supports de cours pour « inférence et analyse de réseaux »](http://www.nathalievialaneix.eu/teaching/network.html) (formation PF biostatistique, Nathalie Vialaneix), avec, en particulier, la [vignette](http://www.nathalievialaneix.eu/doc/pdf//wikistat-network_compiled.pdf")
-	[supports de cours pour « apprentissage statistique »](http://www.nathalievialaneix.eu/teaching/learning.html)  (formation PF biostatistique, Nathalie Vialaneix)
- [supports de cours pour « statistique avancée »](https://github.com/cmaugis/Stat-Avancees---PFB-Toulouse) (formation PF biostatistique, Cathy Maugis et Pierre Neuvial)

<h2>Analyse de données RNA-seq</h2>

- [supports de cours pour « analyse de données RNA-seq »](http://www.nathalievialaneix.eu/teaching/rnaseq.html) (formation PF biostatistique, Ignacio Gonzales, Annick Moisan et Nathalie Vialaneix) avec deux cas d'études complémentaires : [TP1](http://www.nathalievialaneix.eu/doc/html/TP1_normalization.html) et [TP2](http://www.nathalievialaneix.eu/doc/html/TP2_interaction.html)

<h2>Langage de programmation R</h2>

- Initiation à R : [support de cours de Sébastien Déjean](http://perso.math.univ-toulouse.fr/dejean/files/2020/12/intro_R.pdf) avec des versions antérieures [ici](http://www.math.univ-toulouse.fr/~sdejean/PDF/semin-R_juin_2014.pdf) [ici](http://www.math.univ-toulouse.fr/~sdejean/PDF/un-peu-d-R.pdf) et [ici](http://www.math.univ-toulouse.fr/~sdejean/PDF/R-avance.pdf) (avec Thibaut Laurent) et [supports de cours de Cathy Maugis-Rabusseau](https://cmaugis.github.io/TutorielsR) (comprenant Introduction, les bases, la statistique avec R et R avancé)
- Supports de cours pour « graphiques avancés avec R » (formation annuelle PF biostatistique) : [Version 1](http://www.nathalievialaneix.eu/teaching/advanced_graphics.html) (Sébastien Déjean et Nathalie Vialaneix) et la version 2 ([cours](https://perso.math.univ-toulouse.fr/dejean/files/2011/03/slide_ggplot2.pdf) et [TP](https://www.math.univ-toulouse.fr/~sdejean/R/2020/TP_ggplot2.html)) (Sébastien Déjean et Pierre Neuvial)
- [initiation au tidyverse](https://www.math.univ-toulouse.fr/~sdejean/R/2020/TP_tidyverse.html) (Sébastien Déjean)
- [initiation à shiny](https://www.math.univ-toulouse.fr/~sdejean/R/2020/Intro_Shiny.Rpres) (Sébastien Déjean)
- [tutoriel sur l'utilisation de R sur le cluster de la PF Bioinformatique de Toulouse](https://tutorcluster-genotoul-bioinfo-07ea6a4445a9bc29db3a0414e83f9b801.pages.mia.inra.fr/) (cluster slurm) (Gaëlle Lefort, Alyssa Imbert, Julien Henry et Nathalie Vialaneix)
