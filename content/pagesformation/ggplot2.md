---
title: Graphiques avec ggplot2
type: page
date: 15 mai 2025 
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13547/) 

**Description :** La formation aborde la construction de graphiques avec le package `ggplot2` (et quelques packages associés) du logiciel `R`.

**Objectifs :** A la fin de la formation, vous serez capable de comprendre la logique de `ggplot2`, de produire et personnaliser les graphiques les plus couramment utilisés (nuage de points, diagramme en bâtons, boxplot, histogramme...) avec `ggplot2`.

**Mots-clés :** langage R, graphiques

**Notions abordées :**

- grammaire des graphiques
- *aesthetics*, échelles et couches
- représentations graphiques classiques

**Pré-requis :** connaissance du langage R, oublier la façon habituelle de faire des graphiques en `R`

**Durée de la formation :** 1 journée

**Date :** 15 mai 2025

**Lieu :** Institut de Mathématiques de Toulouse

Salle Huron, 
Institut de Mathématiques,
Campus Université Paul Sabatier, 
118 Route de Narbonne,
Batiment 1R1, 
95 rue Sébastienne Guyot
31062 TOULOUSE Cedex


