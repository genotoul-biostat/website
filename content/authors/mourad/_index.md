---
authors:
- mourad
bio: 
first_name: Raphael
last_name: Mourad
organizations:
- name: UPS / CBI
role: MCF
social:
- icon: globe
  icon_pack: fas
  link: https://raphaelmourad.wixsite.com/raphaelmourad
superuser: false
title: Raphael Mourad
user_groups:
- Researchers
external_link: https://raphaelmourad.wixsite.com/raphaelmourad
---

blabla de description
