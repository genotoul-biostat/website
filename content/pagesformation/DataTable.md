---
title: Formation au package R data.table
date: 
type: page
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/12176/) 

**Description** :
Le package  `data.table` s'avère très efficace pour la manipulation de données, en temps et mémoire.  Il permet en particulier de traiter avec R d'importants volumes de données.

Pour plus d'infos : [https://rdatatable.gitlab.io/data.table/](https://rdatatable.gitlab.io/data.table/)

**Notions abordées** :

- Benchmark
- Syntaxe du package
- Points forts du package et points de vigilence
- Modification des objets R par référence

**Prérequis** : La formation s’adresse à des utilisateurs réguliers de R.

**Intervenantes** : Elise Maigne et Marion Aguirrebengoa

**Durée** : 1 jour

<!--
Encore un peu de patience, les inscriptions seront bientôt ouvertes !
Pour s'inscrire, cliquez sur [le formulaire](https://lime.nathalievialaneix.eu/index.php/327138?lang=fr) 
-->