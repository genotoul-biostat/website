---
title: Exemples de stages / projets tutorés impliquant la PF BioStat
type: page
date: 
---

Voici une liste non-exhaustives d’exemples de stages et de projets tutorés dont l’encadrement à impliqué la plateforme Biostatistique :

- **Stages**

  + Juin-Sept 2023 – **Analyse de données de transcriptomique spatiale du foie** <br> 
  I2MC, IMT – 4MA INSA Toulouse.
  
  + Juin-Sept. 2021 – **Prédiction multi-label d’agents pathogènes présents dans un échantillon clinique** <br>
  Dendris, IMT – 4MA INSA Toulouse.
  
  + Juin-Sept. 2021 – **Analyses statistiques de données single-cell RNA-seq**<br> 
  IMT – 4MA INSA Toulouse.
  
  + Avril-Juillet 2021 – **Intégration de données omiques multi-niveaux pour expliquer l’efficacité alimentaire chez les agneaux** <br>
  INRAE – MApI3 UPS.

  + Avril-Juillet 2020 – **Intégration de données omiques pour l’étude de l’impact des contaminants alimentaires sur le métabolisme et le développement de cancers** <br> 
  MIAT, IMT – MApI3 UPS.
  
  + Avril-Juillet 2020 – **Développement d’une application Shiny pour l’exploitation de résultats de classification non supervisée pour des données single-cell RNA-seq** <br> 
IMT, INRAE – MI SID UPS.

  + Fév.-juillet 2019 – **Analyses statistiques de données single-cell RNA-seq : application à l’étude de cellules stromales mésenchymateuses** <br> 
  Restore, IMT – M2.
  
  + Juin-sept 2019 – **Etude de la co-expression des gènes du Medicago Truncatula à partir de données RNA-seq**  <br>
  LIPM, IMT, TBI – 4MA INSA Toulouse.

  + Avril-Juillet 2014 – **Analyse statistique sur le rendement d’un foie gras de canard en fonction de la durée de jeûne et du type de maïs ingéré**<br> 
ENSAT, GenPhySE – M1 IMAT

  + Avril-Juillet 2014 – **Biostatistique de données issues de transcriptomique, protéomique et épigénétique**<br> 
INSERM, CRCT, plateau de protéomique – M1 SID

  + Avril-Juillet 2013 – **Analyse statistique de données biologiques à haut-débit** <br> 
UPS, LBME – M1 IMAT

<br>

- **Projets tuteurés**

  + Oct 2022-Janv 2023: **Statistical analysis of new gene expression data using the ”Spatial Transcriptomic” method** <br> 
  I2MC, IMT – INSA 5MA 

  + Oct 2020 – Juin 2021 : **Evaluation of different statistical learning methods to improve the detection of pathogens in a sample**<br> 
  Dendris, IMT – INSA 5MA et 4MA

  + Oct 2017-Janv 2018 : **Characterization of dairy starters using proteomic data** <br> 
  LISBP, IMT – INSA 5MA

  + Oct 2016-Janv 2017 : **Classification de gènes co-exprimés à partir de données RNA-seq** <br> IMT – INSA 5MA

  + Oct 2016-Janv 2017 : **Etudes des régulations globale et spécifiques de la transcription chez Escherichia coli** <br> LISBP, IMT – INSA 5MA

  + Nov. 2014 – Janv. 2015 – **Prediction of the evolution of insulin resistance of obese individuals during diet** <br> INSERM, I2MC – INSA 5A

  + Janv.-Mai 2014 – **Analyses statistiques de données protéomiques et métabolomiques : étude de la fonte lipidique à la cuisson du foie gras de canard mulard** <br> ENSAT, GenPhySE – INSA 4A
