---
date: "2025-02-19"
title: Formations ouvertes en 2025 
---

Le catalogue des formations pour 2025 est disponible. 

Pour plus d'informations, allez dans l'onglet [Formation]({{<ref "/mainformations/_index.md">}})

Pour vous pré-inscrire, rendez-vous [ici](https://indico.math.cnrs.fr/category/708/) 

Quand le nombre de pré-inscrits est atteint, un mail de confirmation d'ouverture de la formation est envoyé. 
C'est après cette confirmation que les devis sont transmis pour le paiement. 

<!--more-->

