---
banner:
  caption: ""
  image: ""
title: Formations
view: page
---

La plateforme de biostatistique propose une offre de formations qui est mise à jour annuellement. Des modules plus spécifiques sont organisés ponctuellement, comme par exemple, un module sur le traitement bio-informatique et bio-statistique de données RNASeq en collaboration avec la plateforme de bioinformatique.<br>
Vous pouvez aussi retrouver quelques supports de cours sur [cette page]({{< ref "/pagesaux/supportcours.md" >}}).

**Programme pour 2025**

  + **10 et 11 mars 2025** [Initiation à la programmation R]({{< ref "/pagesformation/InitiationR.md" >}}) 
  + **31 mars et 1er avril 2025** [Initiation à la statistique avec R]({{< ref "pagesformation/StatavecR.md" >}})
  + **11 avril 2025** [Initiation à la programmation R "tidy"]({{< ref "/pagesformation/Tidy.md" >}}) 
  + **15 mai 2025** [Graphiques avec R - ggplot2]({{< ref "/pagesformation/ggplot2.md" >}}) 
  + **16 et 17 juin 2025** [Initiation à l'analyse de données single-cell RNA-seq avec Seurat]({{< ref "/pagesformation/Seurat.md" >}}) 
  + **3 et 4 juillet 2025** [Intégration de données avec mixOmics]({{< ref "/pagesformation/mixomics.md" >}})
  + **11 et 12 septembre 2025** [Grand modèle de langage pour la génomique avec python]({{< ref "pagesformation/DeepLearningAvanceLLM.md" >}})
  + **22 et 23 septembre 2025** [Initiation à la statistique avec ASTERICS]({{< ref "pagesformation/Asterics.md" >}})
  + **13 et 14 octobre 2025** [Machine learning et clustering]({{< ref "/pagesformation/Apprentissage.md" >}})
  + **20 et 21 octobre 2025** [Construire son application Shiny]({{< ref "/pagesformation/Shiny.md" >}})
  
<!--
**Programme pour 2024** [(par ici pour s'inscrire !)](https://indico.math.cnrs.fr/category/708/)
  + **20 et 21 mars 2024** [Initiation à la programmation R]({{< ref "/pagesformation/InitiationR.md" >}}) 
  + **25 au 26 mars 2024** [Initiation à la statistique avec ASTERICS]({{< ref "/pagesformation/Asterics.md" >}})  (organisé par la plateforme bioinformatique, voir [ici](https://bioinfo.genotoul.fr/index.php/events/introduction-statistics-with-asterics/))
  + **Annulé** [Initiation à la programmation R "tidy"]({{< ref "/pagesformation/Tidy.md" >}}) 
  + **10-11 juin 2024** [Intégration de données avec mixOmics]({{< ref "/pagesformation/mixomics.md" >}})
  + **17 juin 2024** [Modèle linéaire et modèle linéaire généralisé avec R]({{< ref "/pagesformation/MLG.md" >}}) 
  + **20 juin 2024** [Graphiques avec R - ggplot2]({{< ref "/pagesformation/ggplot2.md" >}}) 
  + **Annulé** Initiation à l'analyse de données single-cell RNA-seq avec Seurat 
  + **18-19 septembre 2024** [Réseaux de neurones et deep learning avec Keras]({{< ref "/pagesformation/DeepLearning.md" >}})
  + **30 septembre au 2 octobre 2024** [Initiation à la statistique avec R]({{< ref "pagesformation/StatavecR.md" >}})
  + **15 octobre 2024** [Initiation au package R data.table]({{< ref "/pagesformation/DataTable.md" >}}) : "high-performance data.frame" 
  + **18 au 20 novembre 2024** [Construire son application shiny]({{< ref "/pagesformation/Shiny.md" >}})
  <br>
  + Machine learning (CART, Random forest, ...)
-->
  
  

**Modalités pour suivre une formation :**

Les pré-inscriptions pour toutes les formations (dans la limite des places disponibles) se font via [la page  suivante](https://indico.math.cnrs.fr/category/708/).
Toute personne qui se pré-inscrit reste disponible pour la formation, on compte sur vous !
Environ un mois avant la formation, un mail de confirmation est envoyé aux personnes pré-inscrites et le paiement doit alors être effectué 
(un mail est envoyé au contact renseigné pour le paiement). 
Si le nombre minimal de pré-inscrits n'est pas atteint, un mail d'annulation de la formation sera envoyé. 


**Tarifs :** 
  + Pour les formations programmées par la plateforme (voir la liste ci-dessus)
    + Académiques : 150€ HT / jour 
    + Secteur privé : 300€ HT / jour 
    
comprenant les pauses café, le repas du midi, les frais de gestion. \
Une attestation de participation pourra vous être délivrée à la suite de la formation. 
  
  + Pour les formations "privatisées" (demande pour 10 à 30 personnes), un forfait de
    + 1500€ HT / jour pour une formation sans TP (un-e intervenant-e)
    + 2000€ HT / jour pour une formation avec TP (deux intervenant-e-s)\
Merci de faire votre demande via l'adresse de contact ci-dessous.   

N’hésitez pas à nous contacter pour toutes questions sur les formations : 
{{% cta cta_link="mailto:biostat-tlse-formation@groupes.renater.fr" cta_text="Contact pour les formations" %}}