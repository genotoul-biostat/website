---
date: "2023-11-08"
title: 30 nov. 2023 - Journée GenoToul Bioinfo / Biostat 
---

La prochaine journée commune des plateformes Biostatistique et Bioinformatique de GenoToul aura lieu le 30 novembre 2023 à INRAE (site d'Auzeville). 

Pour plus d'informations : [https://bioinfo-biostat.sciencesconf.org/](https://bioinfo-biostat.sciencesconf.org/)

<!--more-->

Biostatistics and Bioinformatics play a key role in biology research with a spectacular development in recent years due to technological advances. This meeting is part of a series of meetings and exchanges between regional teams in bioinformatics and biostatistics. It offers to researchers and engineers the opportunity to present their activity on methodological or technical developments. It also gives the opportunity to biologists to present original results obtained from the application of bioinformatics/biostatistics methods. It follows the events organized in December 2009, March 2011, March 2012, June 2014, June 2015, December 2016, December 2018, October 2019, October 2021, December 2022. The 2020 meeting was canceled due to COVID.

This year the meeting will be held on **Thursday, November 30th** at INRAE, Auzeville center, salle de conférences Marc Ridet. It will be maintained as a virtual meeting if sanitary conditions prevent the face-to-face meeting.
