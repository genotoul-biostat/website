---
banner:
  caption: ""
  image: ""
title: Recherche
view: page
---

L’implication de la plateforme Biostatistique dans des projets de recherche est possible selon différents moyens dont :

- le co-encadrement de thèses ([Exemples]({{<ref "/pagesaux/extheses.md">}}))
- le co-encadrement de stages et de projets tutorés ([Exemples]({{<ref "/pagesaux/exstages.md">}})) 
- un partenariat dans le cadre d'un projet déposé en réponse à un appel d'offres ([Exemples]({{<ref "/pagesaux/exprojets.md">}}))

Le démarrage d'un projet de collaboration peut être grandement facilité par le travail d'étudiants co-encadrés en biologie et statistique dans le cadre de projets tuteurés ou de stages. Les animateurs de la plateforme sont en contact étroit avec les enseignant(e)s de filières susceptibles de former des étudiant(e)s intéressé(e)s par ce type de sujets. A Toulouse, il s’agit essentiellement (mais pas exclusivement) des filières suivantes :

- Spécialité Mathématiques Appliquées (MA) portée par le département GMM de l'INSA de Toulouse
- Master en Ingénierie de Statistique et Informatique Décisionnelle (SID) de l'Université Paul Sabatier
- Master en Ingénierie de Mathématiques Appliquées pour l'Ingénierie, l'Industrie et l'Innovation (MApI3) de l’Université Paul Sabatier

Selon la formation et le niveau, les projets tuteurés ont lieu entre octobre et juin; les stages, d'une durée allant de 3 à 6 mois, commencent entre février et juin.

N'hésitez pas à contacter les animateurs de la plateforme pour discuter de vos projets interdisciplinaires.
{{% cta cta_link="/contact/" cta_text="Contacter la plateforme Biostatistique →" %}}

