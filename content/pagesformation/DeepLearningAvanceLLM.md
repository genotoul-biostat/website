---
title: Grand modèle de langage pour la génomique avec python
type: page
date: 11-12 septembre 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13763/) 

**Description :** Cette formation a pour objectif de présenter les grands modèles de langage dans le contexte de la génomique, de la génétique et de la biologie synthétique. 

**Objectifs :** A la fin de la formation, vous serez capable de préentraîner un modèle, le finetuner, prédire l'impact de mutations par zeroshot learning et générer des séquences ADN synthétiques.

**Mots-clés :** Deep learning, large language model (LLM)



**Notions abordées :**

- Deep learning: bases du deep learning, apprentissage supervisé, réseaux de neurones, ...
- Large language model: problématique, transformers, autoapprentissage, problématique biologique

**Pré-requis :**  Programmation python, idéalement des connaissances de base en machine learning

**Intervenant :** Raphaël Mourad
 
**Durée de la formation :** 1 jour et demi

**Lieu :** INRAE Auzeville, salle de formation du bâtiment C8/MIAT


