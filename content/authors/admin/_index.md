---
bio: 
email: ""
first_name: Cathy
highlight_name: false
last_name: Maugis-Rabusseau
organizations:
- name: INSA Toulouse
  url: "https://www.insa-toulouse.fr/"
- name: IMT
  url: "https://www.math.univ-toulouse.fr/fr/"  
role: MCF 
social:
- icon: globe
  icon_pack: fas
  link: https://www.math.univ-toulouse.fr/~maugis/
superuser: false
title: Cathy Maugis-Rabusseau
user_groups:
- Researchers
external_link: https://www.math.univ-toulouse.fr/~maugis/
---



