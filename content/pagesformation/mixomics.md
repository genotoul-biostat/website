---
title: Intégration de données avec mixOmics
type: page
date: 3-4 juillet 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13548/) 

**Description :** La formation aborde le principe de quelques méthodes d'intégration statistique de données et leur mise en oeuvre avec le package `mixOmics` du logiciel `R`.

**Objectifs :** A la fin de la formation, vous serez capable de savoir quelle méthode utiliser pour répondre à des questions d'intégration de données, de mettre en oeuvre ces méthodes avec le logiciel R et d'en interpréter les résultats sous formes numérique et graphique.

**Mots-clés :** analyses statistiques multivariées, analyses supervisées et non supervisées, méthodes PLS

**Notions abordées :**

- Méthodes linéaires
- Analyse en Composantes Principales
- Analyses supervisées et non supervisées
- Méthodes *Projection to Latent Structures* (PLS)
- Analyses multi-blocs et multi-groupes

**Pré-requis :** connaissance du langage R, connaissance des indicateurs statistiques élémentaires (moyenne, variance, corrélation), avoir été confronté à un jeu de données

**Durée de la formation :** 2 journées 

**Dates :** 3 et 4 juillet 2025

**Lieu :** IMT 

Salle Johnson, Institut de Mathématiques,
Campus Université Paul Sabatier, 118 Route de Narbonne,
Batiment 1R1, 95 rue Sébastienne Guyot
31062 TOULOUSE Cedex

