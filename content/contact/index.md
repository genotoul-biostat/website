---
date: "2023-10-3"
title: Contact
type: landing

sections:

  - block: contact
    content:
      address:
        city: Toulouse
        country: France
        country_code: 
        postcode: 
        region: 
        street: 
      appointment_url: 
      autolink: true
      coordinates:
        latitude: ""
        longitude: ""
      directions: 
      email:  biostat-tlse-animateurs@groupes.renater.fr
      office_hours:
      phone: 05 61 55 92 30
      text: Pour contacter les animateurs et animatrices de la plateforme BioStatistique, merci d'utiliser l'adresse suivante 
      title: Contact
    design:
      columns: "1"
      
  - block: hero
    content:
      title: 
      image:
        filename: map1.jpg
      text: |
        <br> Les animateurs et animatrices de la plateforme sont localisé·e·s sur 3 sites différents (Université Paul Sabatier, INSA Toulouse et INRAE Toulouse)
---

