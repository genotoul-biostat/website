---
title: Exemples de thèses impliquant la PF BioStat
type: page
date: 
---

Voici une liste non-exhaustive de thèses ayant impliquées la plateforme Biostatistique :

- 2022- . : M. Briscik – **Development of kernel approaches for the integration of biological data from heterogeneous sources.** <br>
Projet E-Muse (H2020-MSCA-ITN-2020) <br>
co-encadrement Institut Pasteur – IMT

- 2015 – 2018 : A. Imbert – **Intégration de données hétérogènes complexes à partir de tableaux de tailles déséquilibrées**<br> 
co-financement Methodomics – Région Midi-Pyrénées <br>
co-encadrement MIAT (INRA) – I2MC (INSERM)

- 2014-2017 : H. Duruflé – **Production et traitement de données omics hétérogènes au vue de l’étude la plasticité de la paroi chez des écotypes pyrénéens d’A. thaliana** <br>
co-financement PRES Université de Toulouse – Région Midi-Pyrénées <br> co-encadrement LRSV – IMT

- 2013-2016 : V. Sautron – Génétique moléculaire de la réponse au stress et robustesse chez le Porc – co-financement ANR SusOStress (programme BIOADAPT) et Région Midi-Pyrénées – co-encadrement GenPhySE – MIAT (INRA)

- 2013-2016 : V. Voillet – **Approche intégrative du développement musculaire pour identifier des biomarqueurs précoces de survie néonatale** <br> co-financement de la Région Midi-Pyrénées et des départements de Génétique Animale et de Physiologie et Système d'Elevage de l’INRA <br>
co-encadrement dans GenPhySE à l’INRA (équipes GENOROBUST et DYNAGEN)

- 2013-2016 : L. Marmiesse – **Towards systemic modelling of the MYB transcriptional rheostat that regulates defence responses in Arabidopsis thaliana** <br> 
co-financement département SPE INRA – Région Midi-Pyrénées <br>
co-encadrement LIPM – IMT

- 2009-2012 : F. Rohart – **Prédiction phénotypique et sélection de variables en grande dimension dans les modèles linéaires et linéaires mixtes** <br>
co-encadrement  LGC INRA Toulouse – IMT.

- 2005-2008 : K-A Lê Cao – **Outils statistiques pour la sélection de variables et l’intégration de données « omiques »** <br>
co-encadrement INRA Toulouse – IMT

- 2004-2007 : I. Gonzalez – **Analyse canonique régularisée pour des données fortement multidimensionnelles**<br> 
encadrement IMT co-tutelle Université du Vénézuéla.
