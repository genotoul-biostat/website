---
authors:
- dejean
bio: 
first_name: Sébastien
last_name: Déjean
organizations:
- name: UPS / IMT
role: IR
social:
- icon: globe
  icon_pack: fas
  link: https://perso.math.univ-toulouse.fr/dejean/
superuser: false
title: Sébastien Déjean
user_groups:
- Researchers
external_link: https://perso.math.univ-toulouse.fr/dejean/
---

blabla de description
