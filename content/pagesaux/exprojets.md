---
title: Exemples de projets impliquant la PF BioStat
type: page
date: 
---

La plateforme de biostatistique est impliquée plus ou moins directement dans les projets suivants en cours ou terminés :

- 2021-2024 : COLOcATION (ANR JCJC) Fetal maturity at the feto-maternal interface: COntribution of fetaL and maternal genOmes and tissue metAbolism perturbaTIONs
- 2021 : MAGICS (Métaprogramme INRAE) Utilisation d’une approche transcriptomique à l’échelle cellulaire (CITE-seq) pour comprendre la maturation du système immunitaire de l’intestin lors de la transition alimentaire du sevrage
- 2020-2023 : [ASTERICS](https://asterics.miat.inrae.fr/) (PRRI Région Occitanie) A Tool for the ExploRation and Integration of omiCS data.
- 2018-2019 : Projet [Single-Cell](https://www.math.univ-toulouse.fr/~maugis/projects/single-cell-rna-seq/) (TTIL INSA Toulouse)
- 2017-2022 : METAhCOL (ANR)  Impact of low doses mixtures of aromatic hydrocarbon on colon carcinogenesis process : focus on energy metabolism hallmark
- 2014-2017 WallOmic (APR 2014) – Production et traitement de données omics hétérogènes au vue de l’étude la plasticité de la paroi chez des écotypes pyrénéens d’A. thaliana. APR de l’Université Fédérale Toulouse Midi Pyrénées 2014
- 2012-2016 : SYNTHACs (ANR)  Biologie synthétique pour la synthèse de molécules chimiques à haute valeur ajoutée à partir de ressources carbonées renouvelables, 2011-2015. Investissement d’avenir,  Programme Biotechnologies et Bioressources.
- 2010-2012 : Proliphyc (PHRC)  Étude de la Protéomique Liquidienne Pour l’Hydrocéphalie
- 2010-2012 : Protell (INCa) – Biomarkers identification in CNS relapse of diffuse large B cell lymphomas
