---
authors:
- vialaneix
first_name: Nathalie
last_name: Vialaneix
organizations:
  - name: INRAE/MIAT
role: DR
social:
  - icon: globe
    icon_pack: fas
    link: https://www.nathalievialaneix.eu/
superuser: false
title: Nathalie Vialaneix
user_groups:
  - Researchers
external_link: https://www.nathalievialaneix.eu/
---

Statistician specialist in omics data analysis (transcriptomics, Hi-C, metabolomics), kernel and network methods.
