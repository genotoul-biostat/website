---
title: Initiation à la programmation avec R
type: page
date: 10 - 11 mars 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13542/) 

**Description :** 
La formation a pour objectif de présenter les bases de la programmation R. 
Elle s'adresse aux débutants.

**Objectifs :** 
A la fin de la formation, vous serez capable de manipuler les différents objets de R, 
de créer des fonctions simples et des graphes simples en R, ...

**Mots-clés :** Langage R, Rstudio, librairies

**Notions abordées :**

- Initiation et familiarisation avec l’interface RStudio
- Installation et utilisation des bibliothèques R
- Structures de données et manipulation des objets R
- Introduction au langage de programmation : boucle, conditionnement…
- Initiation et sensibilisation aux données propres (tidy data)
- Graphiques élémentaires avec R
- Statistiques élémentaires avec R

**Pré-requis :** aucun. Le module s'adresse aux débutants.

**Durée de la formation :** 2 jours

**Dates :** 10 et 11 mars 2025

**Lieux :** INRAE Auzeville, salle de formation du bâtiment C8/MIAT.




