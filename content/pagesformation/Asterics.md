---
title: Initiation à la statistique avec ASTERICS
type: page
date: 22 et 23 septembre 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13549/) 

**Description :** La formation a pour objectif de rappeler les bases de la statistique descriptive, dont quelques analyses multivariées (ACP et classification non supervisée), et inférentielle. Elle s'adresse à des débutants qui veulent reprendre les bases ou consolider leurs connaissances en statistique. Elle est adaptée pour les débutants qui ne sont pas à l’aise avec la programmation car la mise en oeuvre se fait via l'application web *[ASTERICS](https://asterics.miat.inrae.fr/)*. 

**Notions abordées :**

- statistique descriptive uni- et bi-dimensionnelle (moyenne, variance, caractéristiques de tendance centrale et de dispersion, corrélation, …)
- graphiques statistiques uni- et bi-dimensionnels
- intervalles de confiance et tests de base (Student, Wilcoxon, $\chi^2$, Fisher, …)
- régression linéaire et introduction basique au modèle linéaire 
- ACP et classification non supervisée (classification hiérarchique, k-means)

**Pré-requis :** aucun pour la version "avec ASTERICS" (le module s'adresse aux débutants) 

**Durée de la formation :** 2 jours 

**Dates :** 22 et 23 septembre 2025

**Lieux :** INRAE Auzeville, salle de formation du bâtiment C8/MIAT.

<!--
Encore un peu de patience, les inscriptions seront bientôt ouvertes !

Pour s'inscrire, cliquez sur [le formulaire](https://lime.nathalievialaneix.eu/index.php/327138?lang=fr) 
-->
