---
title: Initiation à la programmation "TIDY"
type: page
date: 11 avril 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13546/) 

**Description :** 
Utiliser les méthodes des packages de la suite tidyverse pour manipuler efficacement les tableaux de données dans R.

**Objectifs :** A la fin de la formation, vous serez capable de manipuler et de formatter vos données afin de les rendre exploitables pour leur analyse et leur visualisation, tout en utilisant des codes facilement lisibles et compréhensibles grâce aux fonctions « tidy ».

**Mots-clés :** gestion de données (data wrangling), tidy data.

**Notions abordées :**

- notion de tidyness
- tidy coding
- Formattage des tables de données
- Manipulation de données : filtrage, selection, pivotage, regroupement...

**Pré-requis :** Connaissance de base du langage R.

**Durée de la formation :** 1 journée 

**Date :** 11 avril 2025

**Lieu :** CBI 

Salle de formation du Centre de Biologie integrative,
Campus Université Paul Sabatier, 118 Route de Narbonne,
Batiment 4R4 Nicole Le Douarin, 169 avenue Marianne Grunberg-Manago
31062 TOULOUSE Cedex


