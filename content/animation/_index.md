---
banner:
  caption: ""
  image: ""
title: Animations
view: page
---

L'animation scientifique en biostatistique voire biomathématique est relativement importante sur la place toulousaine. Citons par exemple

- le [séminaire](https://miat.inrae.fr/event/) de l'unité de Mathématiques et Informatique Appliquées de Toulouse (MIAT),
- le [séminaire Mathématiques pour la biologie](https://indico.math.cnrs.fr/category/564/) de l'Institut de Mathématiques de Toulouse
- le groupe de travail mensuel [« biopuces » d'INRAE](https://biopuces.netlify.app/post/) : groupe de travail organisé par la plateforme biostatistique de Toulouse, qui se réunit mensuellement en petit comité autour de thèmes liés à l’utilisation de la statistique pour les données issues de la biologie à haut-débit.

Les plateformes BioStatistique et Bioinformatique organisent leur [Journée Régionale Annuelle](https://bioinfo-biostat.sciencesconf.org/) : 30 novembre 2023