---
title: Machine learning et clustering
type: page
date: 13 et 14 octobre 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13552/) 

**Description :** Cette formation a pour but de présenter les problématiques de l'apprentissage supervisé et du clustering, et les méthodes les plus classiques pour chacune.

**Objectifs :** A la fin de la formation, vous serez capable de mettre en application des méthodes pour répondre à une problémtique d'apprentissage supervisé (resp. de clustering) et de les comparer. 

**Mots-clés :** Apprentissage supervisé, clustering 

**Notions abordées :**

- Machine learning : les bases du machine learning, CART, Random Forest, ... 
- Clustering : problématique, Kmeans, clustering hiérarchique, modèles de mélnages gaussiens

**Pré-requis :** Quelques bases en statistique et en R

**Durée de la formation :** 2 jours

**Lieu :** INSA Toulouse
Département Génie Mathématiques et Modélisation
135 avenue de Rangueil
31077 TOULOUSE 


