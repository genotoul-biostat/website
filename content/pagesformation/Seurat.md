---
title: Initiation à l'analyse de données single-cell RNA-seq avec Seurat
type: page
date: 16 et 17 juin 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13551/) 

**Description :** La formation a pour but de vous familiariser avec les grandes étapes d'analyse de données single-cell RNA-seq et de les mettre en application à l'aide du package Seurat. 
Rem: cette formation est aussi une bonne base pour l'étude des données de Spatial Transcriptomics 

**Objectifs :** A la fin de la formation, vous serez capable de mener une analyse de données single-cell RNA-seq avec Seurat.

**Mots-clés :** single-cell RNA-seq, clustering, marker genes, integration, Seurat

**Notions abordées :**

- Construction de l'objet Seurat
- Les grandes étapes du pipeline d'analyse de données single-cell RNA-seq
- Leur mise en application avec Seurat

**Pré-requis :** Avoir des bases de programmation avec R

**Durée de la formation :** 1,5 jours

**Dates :** 16 et 17 juin 2025

**Lieu :** INSA Toulouse
Département Génie Mathématiques et Modélisation
135 avenue de Rangueil
31077 TOULOUSE 

