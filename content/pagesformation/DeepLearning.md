---
title: Initiation aux réseaux de neurones et deep learning avec la librairie R Keras
type: page
date: 18 et 19 septembre 2024
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/12174/) 

**Description :** 

Bases du deep learning et application à la génétique.

**Objectifs :** 

La formation a pour objectif la mise oeuvre pratique, avec R (librairie Keras), d’approches de machine learning et de deep learning pour résoudre des problèmes d’apprentissage automatique rencontrées en biologie moléculaire et génétique humaine.

**Mots-clés :**

Deep learning, réseaux convolutifs, génétique.

**Notions abordées :**

    Notion 1 : Predict regulatory elements with ML and DeepL with machine learning approaches (SVM, K-mers, GLM, random forests) (3h) and with Deep Learning approaches (CNN, model interpretation) (3h)
    Notion 2: Application to human genetics: SNPs from Genome-Wide Association Studies, and expression quantitative trait loci (eQTL or eSNP), Prediction of SNP effect using deep learning for variant prioritization and understanding of biological mechanisms (3h) Intervenants : Raphaël Mourad (CBI, Université Toulouse 3, actuellement en délégation à MIAT-MathNum)


**Pré-requis :** 
La formation s’adresse à des utilisateurs réguliers de R.

**Durée de la formation :** 1,5 journée 

**Lieu :** MIAT INRAE Castanet-Tolosan 






