---
title: People
type: landing
date: "2022-10-24"

sections:
- block: markdown
  content:
    title: Membres de la plateforme BioStatistique
    subtitle:
    text: |
      Voici les animateurs·trices de la plateforme Biostatistique. 
  design:
    columns: '1'
    
- block: people
  content:
    sort_ascending: true
    sort_by: Params.last_name
    title: 
    user_groups:
    - Researchers
  design:
    show_interests: false
    show_role: true
    show_social: true
    show_organizations: true
---

Voici les animateurs.trices de la plateforme Biostatistique. <br>

{{% cta cta_link="./contact/" cta_text="Contacter la plateforme Biostatistique →" %}}