---
authors:
- neuvial
bio: 
first_name: Pierre
last_name: Neuvial
organizations:
- name: CNRS / IMT
role: DR
social:
- icon: globe
  icon_pack: fas
  link: https://www.math.univ-toulouse.fr/~pneuvial/
superuser: false
title: Pierre Neuvial
user_groups:
- Researchers
external_link: https://www.math.univ-toulouse.fr/~pneuvial/
---


