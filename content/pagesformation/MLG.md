---
title: Modèle linéaire et modèle linéaire généralisé avec R 
date: 17 juin 2024
type: page
---


Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/11545/) 


**Description** :
La formation s’adresse à des personnes qui ont un niveau initié et veulent approfondir leurs connaissances en statistique. Elle est orientée vers la compréhension des bases de la modélisation par modèles linéaires et modèles linéaires généralisés. 

**Objectifs** : A la fin de la formation, vous serez capable de distinguer les différents modèles linéaires généralisés, comprendre les étapes clés d'estimation des paramètres, tests de sous-modèles, prédiction. Vous serez capable de les implémenter sous R (ou Python) et de comprendre les sorties correspondantes. 

**Mots clés** : Régression linéaire, Analyse de variance (ANOVA), Analyse de covariance (ANCOVA), régression logistique, régression loglinéaire (Poisson) 

**Notions abordées** :

- modèle linéaire (régression, Anova, Ancova) : modélisation, estimation des paramètres, tests de sous-modèle, sélection de variables, ...
- modèle linéaire généralisé (régression logistique, régression Poisson, ...) : modélisation, odds-ratio, tests de sous-modèles, ...

**Pré-requis** : 
Au moins le niveau de la formation "Initiation à la statistique avec R" et une connaissance du langage de programmation R.


**Durée de la formation** : 1 jour

**Lieu** : La formation aura lieu à MIAT Toulouse. 







