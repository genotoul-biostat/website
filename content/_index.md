---
# Leave the homepage title empty to use the site title
title:
date: 2023-10-03
type: landing

sections:
  - block: hero
    content:
      title: |
        Plateforme de Biostatistique
      image:
        filename: LogoPoster.jpg
      text: |
        <br> La plateforme Biostatistique est l'une des 12 plateformes du [GenoToul](https://www.genotoul.fr/) (Génopole Toulouse).<br> 
        C'est un carrefour de compétences en Statistique pour la Biologie.<br>
        Les membres de la plateforme  
          - proposent une offre de formation (mises à jour annuellement)
          - s'impliquent dans des projets interdisciplinaires
          - co-encadrent des stages/thèses/projets 
          - organisent des animations scientifiques     
 
  - block: markdown
    content:
      title:
      subtitle:
      text: |
        {{% cta cta_link="./contact/" cta_text="Contacter la plateforme Biostatistique →" %}}
    design:
      columns: '1'
      
  - block: collection
    content:
      title: News 
      subtitle:
      text:
      count: 5
      filters:
        author: ''
        category: ''
        exclude_featured: false
        publication_type: ''
        tag: ''
      offset: 0
      order: desc
      page_type: post
    design:
      view: compact
      columns: '1'
 
  - block: portfolio
    content:
      title: Quelques faits marquants / projets
      subtitle:
      text:
      count: 5
      filters:
        author: ''
        category: ''
        exclude_featured: false
        publication_type: ''
        tag: ''
      offset: 0
      order: desc
      page_type: faitsmarquants
    design:
      columns: '1'
      view: masonry
      flip_alt_rows: true
      background: {}
      spacing: {padding: [0, 0, 0, 0]}
      

---