---
title: Initiation à la statistique avec R
type: page
date: 31 mars -  1er avril 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13543/) 

**Description :** La formation a pour objectif de rappeler les bases de la statistique descriptive, dont quelques analyses multivariées (ACP et classification non supervisée), et de la statistique inférentielle. Elle s'adresse à des débutants qui veulent reprendre les bases ou consolider leurs connaissances en statistique. Les notions seront illustrées à l'aide du logiciel R.

**Notions abordées :**

- statistique descriptive uni- et bi-dimensionnelle (moyenne, variance, caractéristiques de tendance centrale et de dispersion, corrélation, ...)
- graphiques statistiques uni- et bi-dimensionnels
- intervalles de confiance et tests de base (Student, Wilcoxon, $\chi^2$, Fisher, …)
- régression linéaire et introduction basique au modèle linéaire 
- ACP et classification non supervisée (classification hiérarchique, k-means)

**Pré-requis :** Notions de base de la programmation avec R (a minima, les notions du module d'initiation à la programmation R doivent être acquises)

**Durée de la formation :** 2 jours 

**Dates :** 31 mars et 1er avril 2025

**Lieux :  CBI**

Salle de formation du Centre de Biologie integrative,
Campus Université Paul Sabatier, 118 Route de Narbonne,
Batiment 4R4 Nicole Le Douarin, 169 avenue Marianne Grunberg-Manago
31062 TOULOUSE Cedex

<!--
Pour s'inscrire, cliquez sur [le formulaire](https://lime.nathalievialaneix.eu/index.php/327138?lang=fr) 
-->
