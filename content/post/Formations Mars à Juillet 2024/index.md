---
date: "2024-02-04"
title: Formations ouvertes mars à juillet 2024  
---

Les formations suivantes sont ouvertes (mars à juillet 2024).
  - Initiation à la programmation R
  - Initiation à la programmation R "tidy"
  - Graphiques avec R - ggplot2
  - Modèle linéaire et modèle linéaire généralisé avec R
  - Initiation à l'analyse de données single-cell RNA-seq avec Seurat 

Pour vous inscrire, rendez-vous [ici](https://indico.math.cnrs.fr/category/708/) 

Pour plus d'informations, allez dans l'onglet [Formation]({{<ref "/mainformations/_index.md">}})

<!--more-->

