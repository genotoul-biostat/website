---
title: Construire son application shiny 
type: page
date: 20 et 21 octobre 2025
---

Pour s'inscrire, cliquez sur [le formulaire](https://indico.math.cnrs.fr/event/13550/) 

**Description :** Cette formation est une initiation à R-Shiny qui sert à construire des applications web avec R en restant dans l'écosystème R.
La formation se fera en utilisant RStudio et sera composée en 3 parties:

- Initiation à Shiny avec des exercices
- Construire son application de A à Z (les participants peuvent préparer venir avec leurs idées et leurs données s'ils le souhaitent)
- Bonnes pratiques et déploiement

**Objectifs :** A la fin de la formation, vous connaitrez le fonctionnement de base du package et vous serez capable de construire une application intéractive R-Shiny à partir de scripts R

**Mots-clés :** Application web, R, Visualisation, Interactivité

**Notions abordées :**

- Fonctions de bases Shiny
- Ui/Server
- Observables/Reactives
- Architecture d'un site web
- Représentation graphique intéractive

**Pré-requis :** Connaissance de R

**Durée de la formation :** 2 jours 

**Dates :** 20 et 21 octobre 2025

**Lieu :** INRAE Auzeville, salle de formation du bâtiment C8/MIAT.





