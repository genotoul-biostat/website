# Structure du site web – version Hugo `Academic-starter-research-group`

Dans le dossier `config/_default`

* Fichier `menus.yaml` : pilote le menu de la page web
* Fichier `config.yaml` : 
* Fichier `params.yaml` : 

Dans le dossier `content`

* Fichier `_index.md` : pilote le contenu de la page principale

L'image d'accueil a été construite avec le poster. 
Mise en .jpg pour que cela fonctionne (pb avec .png)

* L'onglet « News » et l'affichage en page principale est alimenté par le dossier « post». Pour chaque (groupe de) formation(s) à venir / news, créer un dossier et un fichier index.md dedans. 

* L'onglet « Membres » est alimenté par le dossier « people » et s'appuie sur « author »\
Mettre des liens vers les pages web des institutions.\

A FAIRE : À homogénéiser 

 * L'onglet « Formations » est alimenté par le dossier `mainformation` pour la gestion de la page principale. Il y a des liens vers les fiches de chaque formation qui sont dans `pagesformation`.
Il y a une fiche modèle `SouchePageFormation.md`. Il y a aussi un lien vers la page `pagesaux/supportcours.md` 

A FAIRE : Continuer à construire les fiches de description. Reprendre les infos de l'ancienne page web. 

 * L'onglet « Animations » est alimenté par le dossier `animation`
 
 * L'onglet « Recherche » est alimenté par le dossier `recherche` et de pages d'exemples dans `pagesaux`
 
 * L'onglet « Contact » est alimenté par le dossier `contact`
 
Je n'ai pas réussi à mettre plusieurs points GPS sur la carte. J'ai donc mis la carte GenoToul mais j'ai dû la couper à la main car je n'arrive pas à bien gérer la taille. 

PB: la map apparait bien localement sur mon ordinateur, n'apparait pas quand je déploie (???)

 * Sur la page principale, il y a les faits marquants / posters : c'est piloté par le dossier `faitsmarquants`.  Il faut faire un dossier par fait marquant. L'image doit s'appeler « featured » et il faut un fichier `index.md`.\
 
A AFIRE : Quand le site web sera fini, il faudrait directement pointer vers le pdf des faits marquants, pour l'instant ça repasse par une fiche. 

A FAIRE : modifier les fins de pages, il y a des logos type twitter, … à supprimer. 

NB: si je (PN) comprends bien les layouts des différentes pages sont [ici](https://forgemia.inra.fr/genotoul-biostat/website/-/tree/main/themes/github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy/v5/layouts).
